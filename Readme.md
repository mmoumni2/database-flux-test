## Maria DB Flux test

In order to effecutate some tests, for testing the flux of mariadb database from the connection until execution of the query and getting the result.

To achieve that  i created this repository to show those tests

to Run this Test you have docker to be installed in your machine and after then follow the instruction below:

1. install requirements: ``` pip install -r requirement.txt```
2. run mariadb container: ``` docker compose up --build``` (you can change database credentials as you want)
3. run the script: ``` python main.py```