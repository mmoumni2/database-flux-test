"""
MariaDB Connector

This module provides a class, MariaDB, for interacting with a MariaDB (MySQL) database using the mysql-connector-python library.

Usage:
    Create an instance of the MariaDB class with the necessary connection details such as hostname, port, username, password, and database name.
    Call the connect() method to establish a connection to the database.
    Use the execute_crud_command() method to execute CRUD (CREATE, READ, UPDATE, DELETE) commands that modify data in the database.
    Use the execute_command() method to execute SQL commands that don't modify data, such as SELECT queries.
    Close the connection by calling the close() method when done.

Example:
    maria_db = MariaDB(
        _hostname="localhost",
        _port="3306",
        _user="root",
        _password="password",
        _database="mydatabase"
    )
    maria_db.connect()
    maria_db.execute_crud_command("INSERT INTO my_table (column1, column2) VALUES ('value1', 'value2');")
"""

import mysql.connector
from dotenv import load_dotenv
import os


class MariaDB:
    def __init__(
        self, _hostname: str, _port: str, _user: str, _password: str, _database: str
    ) -> None:
        """
        Initialize MariaDB instance with connection details.

        Args:
            _hostname (str): MariaDB hostname.
            _port (str): MariaDB port number.
            _user (str): MariaDB username.
            _password (str): MariaDB password.
            _database (str): Name of the database.
        """
        self.hostname = _hostname
        self.port = _port
        self.user = _user
        self.password = _password
        self.database = _database

    def connect(self):
        """
        Connect to the MariaDB database.
        """
        self.connection = mysql.connector.connect(
            host=self.hostname,
            port=self.port,
            user=self.user,
            password=self.password,
            database=self.database,
        )

    def execute_crud_command(self, command: str):
        """
        Execute CRUD (CREATE, READ, UPDATE, DELETE) commands.

        Args:
            command (str): SQL command to execute.
        """
        cursor = self.connection.cursor()
        cursor.execute(command)
        self.connection.commit()
        cursor.close()

    def execute_command(self, command: str):
        """
        Execute SQL command.

        Args:
            command (str): SQL command to execute.

        Returns:
            result (Any): Result of the SQL command.
        """
        cursor = self.connection.cursor()
        cursor.execute(command)
        result = cursor.fetchall()  # Fetch results if needed
        cursor.close()
        return result


# Use try-finally to ensure the connection is always closed
try:
    load_dotenv()
    mariadb_instance = MariaDB(
        _hostname="localhost",
        _port="3306",
        _user="moha",
        _password="moha123456",
        _database="mydatabase",
    )
    
    mariadb_instance.connect()
    mariadb_instance.execute_command("USE mydatabase;")

    # create a table in the database
    mariadb_instance.execute_crud_command(
        "CREATE TABLE IF NOT EXISTS mytable (id INT AUTO_INCREMENT PRIMARY KEY,name VARCHAR(50),age INT);"
    )

    # insert a record to the database
    mariadb_instance.execute_crud_command("INSERT INTO mytable (name, age) VALUES ('anas lbahi', 30);")

    # get the result from the database
    result = mariadb_instance.execute_command("SELECT * FROM mytable;")

    print(f"Result: {result}")

    # create another table
    mariadb_instance.execute_crud_command(
        "CREATE TABLE IF NOT EXISTS employees (id INT AUTO_INCREMENT PRIMARY KEY,first_name VARCHAR(50),last_name VARCHAR(50),email VARCHAR(100),hire_date DATE);"
    )

    mariadb_instance.execute_crud_command(
        "INSERT INTO employees (first_name, last_name, email, hire_date) VALUES ('John', 'Doe', 'john.doe@example.com', '2022-03-30');"
    )
    mariadb_instance.execute_crud_command(
        "INSERT INTO employees (first_name, last_name, email, hire_date) VALUES ('mohamed', 'moumni', 'mohamed.moumni@example.com', '2022-03-30');"
    )
    mariadb_instance.execute_crud_command(
        "INSERT INTO employees (first_name, last_name, email, hire_date) VALUES ('cristiano', 'Ronaldo', 'cristiano.Ronaldo@example.com', '2022-03-30');"
    )
    mariadb_instance.execute_crud_command(
        "INSERT INTO employees (first_name, last_name, email, hire_date) VALUES ('Lionel', 'Messi', 'Lionel.Messi@example.com', '2022-03-30');"
    )
    mariadb_instance.execute_crud_command(
        "INSERT INTO employees (first_name, last_name, email, hire_date) VALUES ('arjen', 'Roben', 'arjen.Roen@example.com', '2022-03-30');"
    )

    result = mariadb_instance.execute_command("SELECT * FROM employees;")
    print(f"Result: {result}")


finally:
    # Close the connection
    mariadb_instance.connection.close()
